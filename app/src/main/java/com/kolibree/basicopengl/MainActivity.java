package com.kolibree.basicopengl;

import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

  private GLSurfaceView surfaceView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    surfaceView = findViewById(R.id.surface);

    // Request an OpenGL ES 2.0 compatible context.
    surfaceView.setEGLContextClientVersion(2);

    // Set the renderer to our demo renderer, defined below.
    surfaceView.setRenderer(new BasicRenderer());
  }

  @Override
  protected void onResume() {
    super.onResume();
    surfaceView.onResume();
  }

  @Override
  protected void onPause() {
    super.onPause();
    surfaceView.onPause();
  }
}
